require 'open-uri'

namespace :parser do
  namespace :proxies do
    desc 'List proxies'
    task list: :environment do
      Proxy.where(active: true).each do |proxy|
        puts proxy.ip
      end
    end

    desc 'Check proxies'
    task check: :environment do
      Proxies.new.check
    end

    desc 'Reactivated proxies'
    task reactivated: :environment do
      Proxy.update_all(active: true)
    end
  end

  namespace :avito do
    desc 'Parsing avito.ru'
    task :start, [:first, :last] => :environment do |t, args|
      first = args[:first] || 1
      last = args[:last] || 25
      Avito::Parser.new(first, last).start
    end

    desc 'Checking avito ads'
    task :check, [:offset] => :environment do |t, args|
      offset = args[:offset] || 0
      Avito::Parser.check(offset: offset)
    end
  end
end
