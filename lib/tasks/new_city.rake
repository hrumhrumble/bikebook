namespace :new_city do
  task go: :environment do
    Bike.all.each do |bike|
      new_city = City.where('lower(name) ILIKE ?', bike.old_city).first

      bike.update(city_id: new_city.id)
      print '.'
    end
  end
end

