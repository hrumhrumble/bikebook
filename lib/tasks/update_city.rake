namespace :update_city do
  task go: :environment do
    Bike.all.each do |bike|
      city = bike.city.name
      department = bike.city.department.name

      bike.update(old_city: city, old_department: department)
      print '.'
    end
  end
end

