SimpleNavigation::Configuration.run do |navigation|
  navigation.items do |primary|
    primary.dom_class = 'navigation'
    primary.item :home, 'Велосипеды', root_path, html: { class: 'left_leaf' }, highlights_on: /^\/$|bikes\/\d+$|commit/

    if user_signed_in?
      if current_user.role == 'admin'
        primary.item :admin, 'Админка', admin_dashboard_path
        primary.item :moderation, 'Модерация', admin_moderation_path
      end
      primary.item :place_new_bike, 'Подать объявление', new_profiles_bike_path
      primary.item :my_bikes, 'Мои объявления', profiles_bikes_path
      primary.item :edit_user_registration, 'Мой профиль', edit_user_registration_path
      primary.item :destroy_user_session, 'Выйти', destroy_user_session_path, class: 'danger', method: :delete
    end
    primary.item :sell_bike, 'Продать велосипед', new_profiles_bike_path, html: { class: 'sell_bike' }, unless: proc {user_signed_in?}
    primary.item :signup, 'Вход', new_user_session_path, class: 'registration', unless: proc {user_signed_in?}
  end
end
