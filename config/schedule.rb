# Learn more: http://github.com/javan/whenever

set :output, "/home/deploy/www/bikebook/shared/log/cron_log.log"

every '0 7-21 * * *' do
  rake 'parser:avito:start\[1,2\]'
end

every '0 1 * * *' do
  rake 'parser:avito:check'
end

every '0 6 * * *' do
  rake 'parser:proxies:check'
end

every '0 0 * * *' do
  rake '-s sitemap:refresh'
end

every '0 10 * * *' do
  runner "Bike.where(status: :closed).destroy_all"
end