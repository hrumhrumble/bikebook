SitemapGenerator::Sitemap.default_host = "http://bikebook.ru"

SitemapGenerator::Sitemap.create do
  City.all.each do |city|
    add city_path(city) if city.bikes.approved.without_companies.size > 1
  end

  Bike.approved.without_companies.find_each do |bike|
    add bike_path(bike.city, bike), lastmod: bike.updated_at, changefreq: 'daily'
  end
end
