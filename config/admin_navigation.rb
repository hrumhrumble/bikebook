SimpleNavigation::Configuration.run do |navigation|
  navigation.items do |primary|
    primary.dom_class = 'navigation admin'
    primary.item :dashboard, 'Дашборд', admin_dashboard_path
    primary.item :moderation, 'Модерация', admin_moderation_path
  end
end
