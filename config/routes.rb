Rails.application.routes.draw do
  root 'cities#show'

  namespace :admin do
    get 'dashboard/index', to: 'dashboard#index', as: :dashboard

    get 'moderation/index', to: 'moderation#index', as: :moderation
    post 'moderation/status_update', to: 'moderation#status_update'
    post 'moderation/company', to: 'moderation#company'
  end

  namespace :profiles do
    resources :bikes, except: [:destroy, :show]
    get 'bikes/:id', to: 'bikes#show', constraints: { id: /\d+/ }

    post 'bikes/:id/restore', to: 'bikes#restore', as: :bike_restore
    post 'bikes/:id/close', to: 'bikes#close', as: :bike_close

    resources :pictures, only: :destroy
    post 'pictures/upload', to: 'pictures#upload'
    patch 'pictures/upload', to: 'pictures#upload'
  end

  get '/bikes/:id', to: redirect('/')

  get '/cities', to: 'cities#index'
  get '/cities/new', to: 'cities#new'
  get '/:city_id/bikes', to: 'cities#show', as: :city
  get '/:city_id/bikes/:id', to: 'bikes#show', as: :bike

  get '/bikes/:id/phone', to: 'bikes#phone'
  get '/bikes/:id/email', to: 'bikes#email'

  get 'phones/send_sms_code', to: 'phones#send_sms_code'
  put 'phones/:id', to: 'phones#update'

  resources :favorites, only: :index do
    post 'set', as: 'set'
    post 'unset', as: 'unset'
  end

  resources :feedback, only: [:create, :new]

  devise_for :users, controllers: { registrations: "registrations" }
end
