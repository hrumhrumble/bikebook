set :scm, :git
set :repo_url, 'git@gitlab.com:hrumhrumble/bikebook.git'
set :user, 'deploy'
set :use_sudo, false
set :rvm_type, :user
set :keep_releases, 3

set :linked_files, %w{config/application.yml config/secrets.yml config/database.yml config/mainsms.yml}
set :linked_dirs, %w{log tmp vendor/bundle public/assets public/system}
set :whenever_identifier, ->{ "#{fetch(:application)}_#{fetch(:stage)}" }

namespace :figaro do
  desc 'SCP transfer figaro configuration to the shared folder'
  task :setup do
    on roles(:app) do
      upload! 'config/application.yml', "#{shared_path}/config/application.yml", via: :scp
      upload! 'config/secrets.yml', "#{shared_path}/config/secrets.yml", via: :scp
      upload! 'config/database.yml', "#{shared_path}/config/database.yml", via: :scp
      upload! 'config/mainsms.yml', "#{shared_path}/config/mainsms.yml", via: :scp
    end
  end

  desc 'Symlink application.yml to the release path'
  task :symlink do
    on roles(:app) do
      execute "ln -sf #{shared_path}/config/application.yml #{current_path}/config/application.yml"
      execute "ln -sf #{shared_path}/config/secrets.yml #{current_path}/config/secrets.yml"
      execute "ln -sf #{shared_path}/config/database.yml #{current_path}/config/database.yml"
      execute "ln -sf #{shared_path}/config/mainsms.yml #{current_path}/config/mainsms.yml"
    end
  end
end

after 'deploy:started', 'figaro:setup'
after 'deploy:symlink:release', 'figaro:symlink'
after 'deploy:publishing', 'deploy:restart'
namespace :deploy do
  task :restart do
    invoke 'unicorn:restart'
  end
end
