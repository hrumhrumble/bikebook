class CreateBikes < ActiveRecord::Migration
  def change
    create_table :bikes do |t|
      t.references :user
      t.string :title
      t.text :description
      t.string :status
      t.string :function
      t.string :wheels
      t.string :gears
      t.string :suspension
      t.string :sex
      t.string :age
      t.integer :price

      t.timestamps null: false
    end
  end
end
