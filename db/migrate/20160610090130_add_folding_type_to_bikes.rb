class AddFoldingTypeToBikes < ActiveRecord::Migration
  def change
    add_column :bikes, :folding, :boolean, default: false
  end
end
