class AddCityToBikes < ActiveRecord::Migration
  def change
    add_reference :bikes, :city
  end
end
