class AddElectricTypeToBikes < ActiveRecord::Migration
  def change
    add_column :bikes, :electric, :boolean, default: false
  end
end
