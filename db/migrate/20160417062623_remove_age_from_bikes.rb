class RemoveAgeFromBikes < ActiveRecord::Migration
  def change
    remove_column :bikes, :age
  end
end
