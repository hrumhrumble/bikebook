class AddOriginalUrlToBike < ActiveRecord::Migration
  def change
    add_column :bikes, :original_url, :string
  end
end
