class AddAgeToBikes < ActiveRecord::Migration
  def change
    add_column :bikes, :age, :string
  end
end
