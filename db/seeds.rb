db_path = File.expand_path("../russia_full.json", __FILE__)
russia_db = open(db_path).read

JSON::parse(russia_db).each do |item|
  department = Department.create!(:name => item["department"], :slug => item["slug"])
  item["cities"].each do |city|
    department.cities.create!(:name => city["name"], :slug => city["slug"])
  end
end
puts 'cities: [OK]'
