module CitiesHelper
  def bookmarked(id: bike.id)
    if cookies[:favorites].present?
      cookies_ary = JSON.parse(cookies[:favorites])
      'set' if cookies_ary.include?(id.to_s)
    end
  end
end
