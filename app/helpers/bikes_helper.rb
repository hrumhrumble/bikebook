module BikesHelper
  def pretty_date(date)
    if date.today?
      'Размещено сегодня'
    elsif date.to_date == Date.yesterday
      'Размещено вчера'
    else
      l(date, format: '%e %B %Y')
    end
  end
end

