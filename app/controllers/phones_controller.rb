class PhonesController < ApplicationController
  before_action :phone_params

  def update
    @phone = Phone.find(@params[:id])
    @sms_code = @params[:sms_code].to_i
    @sms_hash_key = BCrypt::Password.new(current_user.phone.sms_key)

    if @sms_hash_key == @sms_code
      @phone.update(activated: true)
      render json: { message: { success: t(:phone_activated) } }
    else
      render json: { message: { alert: t(:phone_code_wrong) } }, status: 400
    end
  end

  def send_sms_code
    sms_code = Phone.generate_key

    response = MainsmsApi::Message.new(
      sender: 'bikebook',
      message: t(:sms_message, sms_key: sms_code),
      recipients: [current_user.phone.number]
    )
    sms_hash_key = BCrypt::Password.create(sms_code)

    update_sms_key(sms_hash_key)
    sms_status(response)
  end

  private

  def sms_status(response)
    status = response.deliver.status

    if status == 'success'
      render json: { message: { success: t(:message_sent) } }
    else
      render json: {
        message: { alert: t(:message_not_sent) }
      }, status: 400
    end
  end

  def update_sms_key(hashed_key)
    current_user.phone.update(sms_key: hashed_key)
  end

  def phone_params
    @params = params.permit(:id, :sms_code)
  end
end
