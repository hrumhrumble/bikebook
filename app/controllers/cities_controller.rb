# encoding: utf-8
require 'unicode'

class CitiesController < ApplicationController
  def index
    @cities = City.where('lower(name) ILIKE ?', "#{Unicode::capitalize(params[:city].strip)}%").limit(10)
    render json: { results: @cities.map {|city| {id: city.slug, text: city.name}} }
  end

  def new
    @cities = City.where('lower(name) ILIKE ?', "#{Unicode::capitalize(params[:city].strip)}%").limit(10)
    render json: { results: @cities.map {|city| {id: city.id, text: city.name}} }
  end

  def show
    @cities = params[:city_id].present? ? [City.friendly.find(params[:city_id])] : []
    @city   = params[:city_id].present? ? City.friendly.find(params[:city_id]) : nil
    @action = @city.present? ? "/#{@city.try(:slug)}/bikes" : "/"
    filter
    respond_to do |format|
      format.html
      format.json {
        render json: {
          result: @bikes_filtered.count.zero? ? t('bikes.empty_search') : t('bikes.advert', count: @bikes_filtered.count),
          size: @bikes_filtered.count
        }
      }
    end
  end

  private

  def filter
    @bikes = Bike.approved.without_companies.eager_load(:pictures, :city)
    @bikes = @bikes.where('city_id = ?', @city.id) if @city.present?
    @bikes = @bikes.where('function = ?', params[:function]) if params[:function].present?
    @bikes = @bikes.where('age = ?', params[:age]) if params[:age].present?
    @bikes = @bikes.where('sex = ?', params[:sex]) if params[:sex].present?
    @bikes = @bikes.where('folding = ?', params[:folding]) if params[:folding].present?
    @bikes = @bikes.where('electric = ?', params[:electric]) if params[:electric].present?
    @bikes = @bikes.where('suspension = ?', params[:suspension]) if params[:suspension].present?
    @bikes = @bikes.where('price >= ?', format_to_i(params[:price_from])) if params[:price_from].present?
    @bikes = @bikes.where('price <= ?', format_to_i(params[:price_to])) if params[:price_to].present?
    case params[:sort]
      when 'cheap'
        @bikes = @bikes.order(price: :asc)
      when 'expensive'
        @bikes = @bikes.order(price: :desc)
      else
        @bikes = @bikes.order(created_at: :desc)
    end
    @bikes_filtered = @bikes
    @bikes = @bikes.page(params[:page]).per(24)
  end
end
