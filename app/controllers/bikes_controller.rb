class BikesController < ApplicationController
  impressionist actions: [:show]

  def show
    @bike = Bike.approved.without_companies.find_by!(id: params[:id])
    @similar = similar(@bike)
    impressionist(@bike)
    render partial: 'shared/bike_show', locals: {bike: @bike} if params[:modal]
  rescue ActiveRecord::RecordNotFound
    if params[:modal]
      render json: { message: t(:bike_not_found) }, status: 400
    else
      redirect_to(root_path, flash: {error: t(:bike_not_found)})
    end
  end

  def phone
    render json: { number: Bike.find_by(id: params[:id]).phone }
  end

  def email
    render json: { email: Bike.find_by(id: params[:id]).email }
  end

  def similar(bike)
    price = bike.price * 0.20
    Bike.approved.without_companies.includes(:city, :pictures).where(
      function: bike.function,
      price: (bike.price - price)..(bike.price + price),
      city_id: bike.city_id
    ).where.not(id: bike.id).limit(4)
  end
end
