class Profiles::BikesController < ApplicationController
  before_action :load_city
  before_action :drop_session_pictures, except: [:create, :update, :edit]

  def index
    @bikes = policy_scope([:profiles, Bike]).order(
      "status = 'rejected' DESC",
      "status = 'approved' DESC",
      "status = 'on_moderation' DESC",
      "status = 'closed' DESC"
    )
  end

  def new
    @bike = Bike.new
    authorize [:profiles, @bike]
  end

  def show
    @bike = Bike.find_by(id: params[:id])
    authorize [:profiles, @bike]
    impressionist(@bike)
  rescue ActiveRecord::RecordNotFound, Pundit::NotDefinedError
    redirect_to(profiles_bikes_path, flash: {error: t(:bike_not_found)})
  end

  def create
    @bike = Bike.new(bike_params)
    authorize [:profiles, @bike]

    if session[:pictures].present?
      session[:pictures].each do |picture_id|
        @bike.pictures << Picture.find_by(id: picture_id)
      end
    end

    if @bike.save
      AdminMailer.on_moderation.deliver_later if @bike.original_url.blank?
      session[:pictures] = []
      redirect_to(profiles_bikes_path, flash: {success: t(:bike_created)})
    else
      render :new
    end
  end

  def edit
    @bike = Bike.find_by(id: params[:id])
    authorize [:profiles, @bike]
    session[:bike_id] = @bike.id
  end

  def update
    @bike = Bike.find_by(id: params[:id])
    authorize [:profiles, @bike]

    if @bike.update(bike_params)
      AdminMailer.on_moderation.deliver_later if @bike.original_url.blank?
      session[:pictures] = []
      redirect_to(profiles_bikes_path, flash: {notice: t(:bike_updated)})
    else
      render :edit
    end
  end

  def restore
    @bike = Bike.find_by(id: params[:id])
    authorize [:profiles, @bike]

    if @bike.update(status: :on_moderation)
      AdminMailer.on_moderation.deliver_later
      redirect_to(profiles_bikes_path, flash: {notice: t(:bike_restored)})
    else
      flash[:error] = t(:bike_restored_erorr)
    end
  end

  def close
    @bike = Bike.find_by(id: params[:id])
    authorize [:profiles, @bike]

    if @bike.update(status: :closed)
      redirect_to(profiles_bikes_path, flash: {notice: t(:bike_closed)})
    else
      flash[:error] = t(:bike_closed_error)
    end
  end

  def drop_session_pictures
    if session[:pictures].present?
      session[:pictures].each do |picture_id|
        Picture.find_by(id: picture_id).delete
      end
      session[:pictures] = []
    end
    session[:bike_id] = nil
  end

  def load_city
    @city = City.friendly.find(params.dig(:bike, :city_id)) if params[:bike].present? && params[:bike][:city_id].present?
  end

  private

  def bike_params
    params.require(:bike).permit(
      :title,
      :description,
      :function,
      :suspension,
      :folding,
      :electric,
      :age,
      :sex,
      :gears,
      :wheels,
      :price,
      :city_id,
      :user_id
    )
  end
end
