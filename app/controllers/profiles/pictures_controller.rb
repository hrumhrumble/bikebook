class Profiles::PicturesController < ApplicationController
  def destroy
    @picture = Picture.find_by(id: params[:id])
    if @picture.destroy
      session[:pictures] -= [params[:id].to_i] if session[:pictures].present?
      render json: { message: {success: t(:status_update_success)}}
    else
      render json: { message: {alert: t(:status_update_error)}}, status: 400
    end
  end

  def upload
    session[:pictures] ||= []
    picture_id = 0

    if params[:pictures]
      if session[:bike_id].present?
        params[:pictures].each do |picture|
          current_bike = Bike.find_by(id: session[:bike_id])
          if current_bike.pictures.size >= 8
            return false
          else
            bike = current_bike.pictures.create(img: picture)
            picture_id = bike.id
            session[:pictures] << bike.id
          end
        end
      else
        params[:pictures].each do |picture|
          break if session[:pictures].size >= 8
          picture_id = Picture.create(img: picture).id
          picture_id = picture_id
          session[:pictures] << picture_id
        end
      end
    end

    picture_preview = Picture.find_by(id: picture_id)

    # json data for jquery-file-upload
    render json: {
      pictures: session[:pictures], # pictures array
      picture_id: picture_id,
      picture_preview: picture_preview.img.url(:medium_s)
    }
  end
end
