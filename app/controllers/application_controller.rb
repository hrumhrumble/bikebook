class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  include Pundit
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  def is_number?(string)
    true if Float(string) rescue false
  end

  def favorites_size
    favorites = cookies[:favorites].present? ? JSON.parse(cookies[:favorites]).select { |s| is_number?(s) } : []
    if favorites.size > 0
      favorites.size
    else
      ''
    end
  end
  helper_method :favorites_size

  def after_sign_in_path_for(resource)
    if current_user.role == 'admin'
      root_path
    elsif current_user.bikes.any?
      profiles_bikes_path
    else
      root_path
    end
  end

  def format_to_i(price)
    price.gsub(/\s+/, '').to_i
  end

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  private

  def user_not_authorized
    flash[:alert] = t(:user_not_authorized)
    redirect_to(root_path)
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up,         keys: [[phone_attributes: [:id, :number, :_destroy]], :name])
    devise_parameter_sanitizer.permit(:account_update,  keys: [[phone_attributes: [:id, :number, :_destroy]], :name])
  end
end
