class FeedbackController < ApplicationController
  def new
    @feedback = Feedback.new
    authorize @feedback
  end

  def create
    @feedback = Feedback.new(feedback_params.merge(user: current_user))
    authorize @feedback

    if @feedback.save!
      AdminMailer.feedback(@feedback).deliver_later
      flash[:success] = t(:message_sent)
      redirect_to root_path
    else
      render :new
    end
  end

  def feedback_params
    params.require(:feedback).permit(:title, :description)
  end
end
