class Admin::ModerationController < ApplicationController
  def index
    @bikes = Bike.on_moderation.without_companies.order(id: :desc).page(params[:page]).per(20)
    authorize [:admin, :moderation], :index?
  end

  def status_update
    @bike = Bike.find_by(id: params[:id])
    authorize [:admin, :moderation], :status_update?
    params[:status] == 'company' ? is_company(@bike) : is_status(@bike)
  end

  private

  def is_company(bike)
    if bike.user.update(is_company: true)
      render json: { message: t('bike.status.success', status: params[:status]) }, status: 200
    else
      render json: { message: t('bike.status.error', status: params[:status])}, status: 400
    end
  end

  def is_status(bike)
    if bike.update_attributes!(status: params[:status])
      UserMailer.rejected(bike).deliver_later if bike.status == :rejected && bike.original_url.blank?
      UserMailer.approved(bike).deliver_later if bike.status == :approved && bike.original_url.blank?
      render json: { message: t('bike.status.success', status: params[:status]) }, status: 200
    else
      render json: { message: t('bike.status.error', status: params[:status])}, status: 400
    end
  end
end
