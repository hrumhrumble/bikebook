class Admin::DashboardController < ApplicationController
  def index
    authorize [:admin, :dashboard], :index?
  end
end
