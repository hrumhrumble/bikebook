class FavoritesController < ApplicationController
  before_action :load_cookies

  def index
    @active, @sell = [], []

    @cookies.each do |i|
      begin
        @active << Bike.approved.without_companies.find(i)
      rescue ActiveRecord::RecordNotFound
        begin
          @sell << Bike.find(i)
        rescue ActiveRecord::RecordNotFound
          next
        end
      end
    end

    return @sell, @active
  end

  def set
    favorites(@cookies << params[:favorite_id])
    render json: { size: favorites_size }
  end

  def unset
    favorites(@cookies.reject{ |e| e == params[:favorite_id] })
    render json: { size: favorites_size }
  end

  private

  def favorites(data)
    cookies.permanent[:favorites] = JSON.generate(data)
  end

  def load_cookies
    @cookies =
      if cookies[:favorites].present?
        JSON.parse(cookies[:favorites])
      else
        JSON.parse(favorites([]))
      end
  end
end
