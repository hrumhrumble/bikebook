class Records
  def initialize(bike)
    @bike = bike
  end

  def create
    user = new_user(@bike)

    if User.find_by(email: user.email).present?
      user = User.find_by(email: user.email)
    else
      user.save!
    end

    bike = create_bike(@bike, user)

    true if bike.save!
  end

  def new_user(bike)
    user = User.new(
      email: bike[:phone] + '@bikebook.ru',
      password: ENV['user_password'],
      password_confirmation: ENV['user_password'],
      confirmed_at: Time.new,
      name: bike[:name]
    )

    user.build_phone(
      number: bike[:phone],
      activated: true
    )
    user
  end

  def create_bike(bike, user)
    record = user.bikes.new(
      title: bike[:title],
      description: bike[:description],
      function: bike[:function],
      price: bike[:price],
      city_id: bike[:city],
      suspension: bike[:suspension],
      sex: bike[:sex],
      age: bike[:age],
      electric: bike[:electric],
      gears: bike[:gears],
      wheels: bike[:wheels],
      folding: bike[:folding],
      original_url: bike[:original_url],
      status: :approved
    )

    bike[:pictures].each do |url|
      record.pictures.new(img: open(url, read_timeout: 10))
    end

    record
  end
end
