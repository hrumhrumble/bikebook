module Avito
  module Fields
    def price(bike)
      bike.css('.price-value').text.gsub(/[^\d+]/, '').to_i
    end

    def title(bike)
      bike.css('.single-item-header').text.strip
    end

    def name(bike)
      bike.css('.person-name').text.strip
    end

    def phone(bike, link, proxy)
      sleep 3
      phone_link = bike.css('.action-show-number')
      options = { headers: { 'Referer' => link } }
      options = options.merge(proxy)
      phone = JSON.parse(
        HTTParty.get('https://m.avito.ru' + phone_link.first['href'] + '?async', options).body
      )
      phone['phone'].gsub(/[^\d+]/, '')
    end

    def description(bike)
      bike.css('.description-preview-wrapper').children
    end

    def gears(bike)
      type = bike.css('.b-single-item').text
      gears = /(\d+)\s?скоро|скоро.*?(\d+)|(\d+)\s?передач|передач.*?(\d+)/i

      result = type.scan(gears).flatten.reject {|v| v.nil?}.first

      result if Bike.gears.values.include?(result)
    end

    def wheels(bike)
      type = bike.css('.b-single-item').text
      wheels = /кол[е|ё]са?.*?(\d+)|диски.*?(\d+)/i

      result = type.scan(wheels).flatten.reject {|v| v.nil?}.first

      result if Bike.wheels.values.include?(result)
    end

    def function(bike)
      type = bike.css('.b-single-item').text
      street_dirt = /дерт|стрит/i
      trial = /триал|трюк/i
      fat = /FAT|фэт|Фет/i
      mtb = /горны[ей]|MTB|МТВ/i
      road = /дорожны/i
      highway = /шоссей/i
      bmx = /bmx|бмх/i
      b29er = /29er/
      hybrid = /гибридн/i
      fixed = /fixed|фиксед/i
      cruiser = /круиз|чоппер/i
      tricycle = /трехколе/i
      exception = /Запчасти и аксессуары|литье|литым|литых|самокат|прокат|наличии|менеджер/i

      case type
        when exception    then :no_type
        when street_dirt  then :street_dirt
        when trial        then :trial
        when hybrid       then :hybrid
        when highway      then :highway
        when b29er        then :_29er
        when fat          then :fat
        when mtb          then :mtb
        when bmx          then :bmx
        when road         then :road
        when fixed        then :fixed
        when cruiser      then :cruiser
        when tricycle     then :tricycle
        else :mtb
      end
    end

    def age(bike)
      type = bike.css('.b-single-item').text
      junior = /подростк|юниор/i
      kid = /детский|ребенк|ребят|ребёнк|девочка|мальчик|ребено|возраст/i

      case type
        when junior       then :junior
        when kid          then :kid
        else :adult
      end
    end

    def electric(bike)
      type = bike.css('.b-single-item').text
      electric = /электро/i

      case type
        when electric then true
        else false
      end
    end

    def folding(bike)
      type = bike.css('.b-single-item').text
      folding = /складн/i

      case type
        when folding then true
        else false
      end
    end

    def city(bike)
      cities = bike.css('.avito-address-text').text
      cities = cities.strip.split(', ')

      cities.each do |city|
        place = City.find_by(name: city)
        if place.present?
          break place.id
        else
          nil
        end
      end
    end

    def pictures(bike)
      pictures ||= []
      if bike.css('#itemPhotogalleryData').text.present?
        p = bike.css('#itemPhotogalleryData').text
        pictures << JSON.parse(p)['photos'].map{|photo| photo[1]}
        full_size(pictures)
      else
        pictures
      end
    end

    def full_size(pictures)
      pictures.flatten.compact.map do |url|
        url.gsub(/\/\/|640x480/, '//' => 'https://', '640x480' => '1280x960')
      end
    end

    def sex(bike)
      source = bike.css('.b-single-item').text
      women = /девоч|женщ|lady/i

      case source
        when women then :female
        else :unisex
      end
    end

    def suspension(function, bike)
      source = bike.css('.b-single-item').text
      full_suspension = /двухподв/i

      return :hard_tail if function == :mtb

      case source
        when full_suspension then :full_suspension
        else :no_suspension
      end
    end

    def hashed(bike, link, proxy)
      function = function(bike)
      age = age(bike)
      folding = folding(bike)

      {
        title: title(bike),
        description: description(bike),
        function: age == :kid ? :road : function,
        price: price(bike),
        suspension: age == :kid ? :no_suspension : suspension(function, bike),
        name: name(bike),
        city: city(bike),
        age: folding ? :junior : age,
        electric: electric(bike),
        folding: folding,
        gears: function == :bmx || age == :kid ? 1 : gears(bike),
        wheels: function == :bmx ? 20 : wheels(bike),
        phone: phone(bike, link, proxy),
        sex: sex(bike),
        pictures: pictures(bike),
        original_url: 'https://avito.ru' + link,
      }
    end
  end
end
