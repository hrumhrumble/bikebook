module Avito
  module Validations
    def filtered?(bike, link, total, index, proxy)
      case true
        # when company?(bike, link, proxy) then status(link, total, index, :banned); puts "\n"; true
        when exist?(link) then status(link, total, index, :record_exist); puts "\n"; true
        when type?(bike) then status(link, total, index, :no_type); puts "\n"; true
        when city?(bike) then status(link, total, index, :no_city); puts "\n"; true
        when pictures?(bike) then status(link, total, index, :no_pictures); puts "\n"; true
        else false
      end
    end
    #
    # def company?(bike, link, proxy)
    #   phone = Phone.find_by(number: phone(bike, link, proxy))
    #   phone.user.is_company? if phone.try(:user).present?
    # end

    def exist?(link)
      Bike.find_by(original_url: 'https://avito.ru' + link).present?
    end

    def type?(bike)
      function(bike) == :no_type
    end

    def city?(bike)
      city(bike).is_a?(Array)
    end

    def pictures?(bike)
      pictures(bike).blank?
    end

    def status(link, total, index, type)
      status = case type
          when :waiting       then '[waiting]'.colorize(:magenta)
          when :start_parsing then '[parsing]'.colorize(:blue)
          when :connection    then '[connect]'.colorize(:red)
          # when :company       then '[company]'.colorize(:red)
          when :no_type       then '[no type]'.colorize(:red)
          when :no_city       then '[no city]'.colorize(:red)
          when :no_pictures   then '[no pics]'.colorize(:red)
          when :record_exist  then '  [exist]'.colorize(:red)
          when :parsed        then ' [parsed]'.colorize(:green)
          when :created       then '[created]'.colorize(:green)
          else                     '  [error]'.colorize(:red)
        end
      print "#{status} [#{index} of #{total}] https://avito.ru#{link}\r"
    end
  end
end
