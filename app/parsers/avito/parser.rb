require_relative '../proxies'
require_relative '../records'

module Avito
  class Parser
    include Fields
    include Validations

    def initialize(first, last)
      @pages = (first..last)
    end

    def self.check(offset: 0)
      bikes = Bike.approved.without_companies.parsed
      bikes_size = bikes.size
      bikes.offset(offset).each.with_index do |bike, index|
        sleep 5
        begin
          counter = "[#{index + 1 + offset.to_i} of #{bikes_size}]"
          response = HTTParty.get(bike.original_url, Proxies.new.take)
          response_url = URI.parse(response.request.last_uri.to_s).path
          bike_url = URI.parse(bike.original_url).path

          if response.code == 200 && response_url == bike_url
            puts "[available] #{counter} #{bike.original_url}".colorize(:green)
          else
            puts "[closed] #{counter} #{bike.original_url}".colorize(:red)
            Bike.find(bike).update(status: :closed)
          end
        rescue OpenSSL::SSL::SSLError, Errno::ECONNRESET
          next
        end
      end
    end

    def start
      @pages.each do |page|
        urls = Urls.new(page, Proxies.new.take).get
        total = urls.size

        puts "Page: #{page}"

        urls.each.with_index do |link, index|
          index += 1
          attempt = 0
          proxy = Proxies.new.take

          status(link, total, index, :waiting)
          sleep 5

          status(link, total, index, :start_parsing)

          begin
            response = HTTParty.get('https://m.avito.ru' + link, proxy)
            bike = Nokogiri::HTML(response)
          rescue StandardError => error
            attempt += 1
            if attempt <= 3
              retry
            else
              status(link, total, index, :connection)
              next
            end
          end

          next if filtered?(bike, link, total, index, proxy)

          begin
            data = hashed(bike, link, proxy)
          rescue StandardError => error
            next
          end

          status(link, total, index, :parsed)

          if Records.new(data).create
            status(link, total, index, :created)
          end

          puts "\n"
        end
      end
    end
  end
end
