module Avito
  class Urls
    def initialize(page, proxy)
      @page = page
      @proxy = proxy
    end

    def page_source(page)
      options = { query: { p: page, bt: 0, user: 1, view: 'list' } }
      options = options.merge(@proxy)
      response = HTTParty.get("https://m.avito.ru/rossiya/velosipedy/", options)
      Nokogiri::HTML(response.body)
    end

    def get
      page_source(@page).css('.item-link').map { |a| a['href'] }
    end
  end
end
