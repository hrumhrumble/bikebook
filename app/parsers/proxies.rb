class Proxies
  def initialize
    @proxies = Proxy.active.map { |proxy| proxy.ip }
  end

  def ban(proxy)
    Proxy.find_by(ip: proxy).update(active: false)
  end

  def check
    @proxies.each do |proxy|
      sleep 5
      if alive?(proxy)
        puts '[passed] '.colorize(:green) + proxy
      else
        puts '[banned] '.colorize(:red) + proxy
        ban(proxy)
      end
    end
  end

  def alive?(proxy)
    options = uri(proxy).merge(timeout: 5)
    begin
      response = HTTParty.get('https://m.avito.ru', options)
    rescue StandardError => error
      return false
    end
    response.code == 200
  end

  def uri(proxy)
    uri = URI.parse('http://' + proxy)
    {
      http_proxyaddr: uri.host,
      http_proxyport: uri.port
    }
  end

  def take
    uri(@proxies.sample)
  end
end