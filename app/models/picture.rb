class Picture < ActiveRecord::Base
  belongs_to :bike

  has_attached_file :img,
                    styles: {
                        big: '1680x1050>',
                        medium_l: "600x500#",
                        medium: "300x250#",
                        medium_s: "200x250#",
                        thumb: "100x150#"
                    },
                    convert_options: {
                        all: "-gravity south -crop 100%x100%+0+50 +repage"
                    }

  validates_attachment_presence :img
  validates_attachment_content_type :img, :content_type => /\Aimage\/.*\Z/
  validates_attachment_size :img, :less_than => 10.megabytes
end
