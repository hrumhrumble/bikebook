class Bike < ActiveRecord::Base
  is_impressionable
  extend Enumerize

  belongs_to :user
  belongs_to :city

  enumerize :status, in: [:on_moderation, :approved, :closed, :rejected], default: :on_moderation
  enumerize :function, in: [
    :mtb,
    :road,
    :hybrid,
    :street_dirt,
    :bmx,
    :downhill,
    :trial,
    :touring,
    :highway,
    :fixed,
    :fat,
    :tandem,
    :recumbent,
    :cruiser,
    :tricycle,
    :exotic
  ]
  enumerize :suspension, in: [:hard_tail, :full_tail, :no_suspension]
  enumerize :age, in: [:kid, :junior, :adult]
  enumerize :sex, in: [:unisex, :female]
  enumerize :gears, in: 33.downto(1).to_a
  enumerize :wheels, in: ['29', '28', '27.5', '27', '26', '24', '20', '18', '16', '14', '12']

  has_many :pictures, dependent: :destroy
  accepts_nested_attributes_for :pictures, allow_destroy: true

  validates :title,
            :function,
            :price,
            :pictures,
            :user,
            :city_id,
            presence: true

  validates_numericality_of :price

  before_update :drop_moderation

  scope :approved, -> { where(status: :approved) }
  scope :on_moderation, -> { where(status: :on_moderation) }
  scope :without_companies, -> { includes(:user).where(users: { is_company: false }) }
  scope :parsed, -> { where.not(original_url: nil) }

  def phone
    user.phone.number
  end

  def email
    user.email
  end

  def price=(new_price)
    self[:price] = new_price.to_s.gsub(/\s+/, '').to_i if new_price.present?
  end

  private

  def drop_moderation
    if title_changed? || description_changed?
      self.status = :on_moderation
    end
  end
end
