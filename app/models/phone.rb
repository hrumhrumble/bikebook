class Phone < ActiveRecord::Base
  belongs_to :user
  validates_presence_of :number
  validates_uniqueness_of :number, conditions: -> { where(activated: true).where.not(sms_key: nil) }

  before_validation :phone_format
  before_update :drop_activation

  private

  def self.generate_key
    rand(1111..9999)
  end

  def phone_format
    self.number = number.gsub(/[\s()-]/, '') if number.present?
  end

  def drop_activation
    self.activated = 'false' if number_changed?
  end
end
