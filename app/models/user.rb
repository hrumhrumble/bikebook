class User < ActiveRecord::Base
  extend Enumerize

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  enumerize :role, in: [:user, :admin], default: :user

  has_many :bikes, dependent: :destroy
  has_many :feedbacks, dependent: :destroy

  has_one :phone, dependent: :destroy
  accepts_nested_attributes_for :phone, allow_destroy: true

  validates :email, :name, presence: true

  after_create do
    AdminMailer.new_user.deliver_later unless email.include?('@bikebook.ru')
  end

  def update_without_password(params, *options)
    if params[:password].blank?
      params.delete(:password)
      params.delete(:password_confirmation) if params[:password_confirmation].blank?
    end

    result = update_attributes(params, *options)
    clean_up_passwords
    result
  end
end
