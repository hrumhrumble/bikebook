class Proxy < ActiveRecord::Base
  validates_uniqueness_of :ip

  scope :active, -> { where(active: true) }
end
