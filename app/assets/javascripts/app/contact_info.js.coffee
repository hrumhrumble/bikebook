class App.ContactInfo
  constructor: (type, btn) ->
    @btn      = $(btn)
    @type     = type
    @root     = @btn.parents('[data-root]')
    @id       = @root.data('id')
    @contact  = @btn.parents('.contact')

  format: (data, type) ->
    return data.number.replace(/(\d)(\d\d\d)(\d\d\d)(\d\d)(\d\d)/, '$1 ($2) $3-$4-$5') if type == 'phone'
    return data.email if type == 'email'

  sendRequest: ->
    $.ajax
      url: "/bikes/#{@id}/#{@type}"
      type: "GET"
      dataType: 'json'
      beforeSend: =>
        @btn.attr('data-loading', 'true')
      success: (data) =>
        @btn.attr('data-loading', 'false')
        @btn.hide()
        @contact.text(@format(data, @type))
      error: =>
        @btn.attr('data-loading', 'false')
        alert('Перезагрузите страницу и попробуйте еще раз')

$ ->
  $(document).on 'click', '[data-behavior=show_contact_phone]', ->
    new App.ContactInfo('phone', @).sendRequest()

  $(document).on 'click', '[data-behavior=show_contact_email]', ->
    new App.ContactInfo('email', @).sendRequest()
