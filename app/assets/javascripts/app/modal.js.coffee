class App.Modal
  constructor: (el) ->
    @el       = $(el)
    @root     = @el.parents('[data-root]')
    @url      = @el.attr('href')
    @image    = @el.find('[data-image]')
    @content  = $('[data-behavior=modal_content]')

  load: ->
    $.ajax
      url: @url
      type: "GET"
      dataType: 'html'
      data: { modal: true }
      beforeSend: =>
        @image.attr('data-loading', 'true')
      success: (data) =>
        @image.attr('data-loading', 'false')
        @open(data)
      error: (data) =>
        @image.attr('data-loading', 'false')
        @close()
        alert($.parseJSON(data.responseText).message)

  open: (data) ->
    @content.html(data)
    $('html').attr('data-modal-open', 'true')
    @content.scrollTop(0)

    @reInit()
    @history(@url)

  close: ->
    $('html').attr('data-modal-open', 'false')
    @history('/')

  reInit: ->
    new App.Moderation().init() if $('.moderator_area').length
    App.Sliders()
    
  history: (url) ->
    window.history.pushState({}, '', url)

$ ->
  $(document).on 'click', '[data-behavior=modal]', (e) ->
    return true if $(window).width() <= 1050
    e.stopPropagation()
    e.preventDefault()
    new App.Modal(@).load()

  $(document).on 'click', '[data-behavior=modal_close]', ->
    new App.Modal().close()

  $(document).on 'click', (e)->
    new App.Modal().close() if $(e.target).is('[data-behavior=modal_content]')

  # FIXME need popstate for back, prev

