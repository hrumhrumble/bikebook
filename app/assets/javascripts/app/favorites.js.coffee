class App.Favorites
  constructor: (action, btn) ->
    @action   = action
    @btn      = $(btn)

    @root     = @btn.parents('[data-root]')
    @id       = @root.data('id')
    @counter  = $('[data-behavior=show_favorites_size]')
    @buttons  = $('[data-behavior=add_to_favorites]')

  setClass: ->
    @buttons.each (i, el) =>
      root = $(el).parents('[data-root]')
      if @id == root.data('id')
        return $(el).addClass('set') if @action == 'set'
        return $(el).removeClass('set') if @action == 'unset'


  favoritesPage: ->
    page        = $('.favorites_index')
    sellItems   = page.find('.sell_items')
    activeItems = page.find('.active_items')

    if page.find('a').size() == 0
      activeItems.remove()
      sellItems.remove()
      page.append($("<div class='no_bookmarks'></div>"))

    sellItems.remove() if sellItems.find('a').size() == 0
    activeItems.remove() if activeItems.find('a').size() == 0

  sendRequest: ->
    $.ajax
      url: "/favorites/#{@id}/#{@action}"
      type: 'POST',
      dataType: 'json',
      beforeSend: =>
        @btn.attr('data-loading', 'true')
      success: (data) =>
        @btn.attr('data-loading', 'false')
        @setClass()
        @counter.text(data.size)

        @buttons.each (i, el) =>
          root = $(el).parents('[data-root]')
          if @id == root.data('id')

            if $('.favorites_index').length  # remove only in favorite
              new App.Modal().close()
              root.remove()

        @favoritesPage()
      error: (data) =>
        @btn.removeClass('loading')
        alert data.responseJSON.error

$ ->
  $(document).on 'click', '[data-behavior=add_to_favorites]', (e) ->
    e.stopPropagation()
    e.preventDefault()
    new App.Favorites('set', @).sendRequest() if !$(@).hasClass('set')
    new App.Favorites('unset', @).sendRequest() if $(@).hasClass('set')
