class App.Moderation
  constructor: (btn) ->
    @btn    = $(btn)
    @root   = @btn.parents('[data-root]')
    @form   = @root.find('form')
    @id     = @root.data('id')
    @status = @btn.data('status')

  init: ->
    $('.moderator_area select').select2
      width: '100%'

  setStatus: ->
    $.ajax
      url: '/admin/moderation/status_update'
      type: 'POST'
      dataType: 'json'
      data: {
        status: @status,
        id: @id
      }
      beforeSend: =>
        @btn.attr('data-loading', 'true')
      success: (data) =>
        @btn.attr('data-loading', 'true')
        @form.submit() if @status == 'approved'
        @root.hide()
        new App.Modal().close() if $('html').data('modal') == true
      error: =>
        @btn.attr('data-loading', 'false')

$ ->
  new App.Moderation().init()

  $(document).on 'click', '[data-behavior=set_status]', (e) ->
    e.stopPropagation()
    e.preventDefault()
    new App.Moderation(@).setStatus()
