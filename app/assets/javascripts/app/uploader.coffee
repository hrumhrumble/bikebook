class App.Uploader
  constructor: (form) ->
    that = @
    @form = $(form)
    @progressBar = @form.find('.progress_bar')

    $('.file_pictures').on 'click', ->
      $('.file_pictures_original').click()

    $('.pictures_list').on 'click', '.remove_picture', ->
      that.removePictures(this)

    @minimumPictures()
    @dropZone()
    @upload()

  minimumPictures: ->
    if $('.edit_fields .fields').length <= 1
      $('.edit_fields').find('.remove_picture').hide()
    else
      $('.edit_fields').find('.remove_picture').show()
    $('.edit_fields .fields').on 'click', '.remove_picture', ->
      if $('.edit_fields .fields:visible').length <= 2
        $('.edit_fields').find('.remove_picture').hide()

  maximumPictures: (data) ->
    addedFiles = data.originalFiles.length
    currentPictures = $('.edit_fields .fields').length

    if (addedFiles + currentPictures) > 8
      alert 'Разрешено загружать всего не более 8 изображений.'
      return false
    else if data.files[0].size > 10000000
      alert 'Максимальный размер файла не больше 10мб'
      return false
    else
      data.submit()

  dropZone: ->
    @form.on 'dragbetterenter', ->
      $(this).addClass('ready_to_drop')
    @form.on 'dragbetterleave', ->
      $(this).removeClass('ready_to_drop')

  removePictures: (picture) ->
      el = $(picture)
      id = el.data('id')
      $.ajax
        url: '/profiles/pictures/' + id
        type: 'POST',
        dataType: 'json',
        data: {"_method": "delete"}
        beforeSend: ->
          el.addClass('loading')
        success: (data)->
          el.parents('.fields').remove()
        error: ->
          el.removeClass('loading')

  progressStart: (data) ->
    progress = parseInt(data.loaded / data.total * 100, 10)
    @progressBar.show().css(width: progress + '%')
    @progressBar.data('progress', progress)

  progressStop: (data) ->
    if @progressBar.data('progress') == 100
      @progressBar.fadeOut(->
        @progressBar.css(width: 0).delay(300).queue((next) ->
          $(this).show()
          next()
        )
      )

  addPictures: (data) ->
    i = 0
    that = @
    $.each data.files, (index, file) ->
      preview = data.result.picture_preview
      id = data.result.picture_id
      tpl = $("<div class='fields field_#{i}' style='background-image: url(#{preview})'><div class='remove_picture' data-id='#{id}'></div></div>")
      $('.pictures_list').append(tpl)
      i++
      that.minimumPictures()

  upload: ->
    $('.file_pictures_original').fileupload
      dataType: 'json'
      dropZone: @form
      limitConcurrentUploads: 1
      acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
      sequentialUploads: true
      url: '/profiles/pictures/upload'
      type: 'POST'
      add: (e, data) =>
         @maximumPictures(data)

      progressall: (e, data) =>
        @progressStart(data)

      done: (e, data) =>
        @addPictures(data)
        @progressStop(data)

      fail: (e, data) =>
        alert 'Произошла неизвестная ошибка. Попробуйте повторить запрос позже.'

$ ->
  new App.Uploader('.bike_new')
