class App.PhoneConfirmation
  constructor: ->
    that = @
    @pc = $('.phone_confirmation')
    @errors = @pc.find('.errors')
    @phone_id = @pc.data('phone-id')
    @check_sms_btn = @pc.find('.check_sms_btn')

    @pc.keypress (e) ->
      if e.which == 13 && $('.check_sms_input').is(':focus')
        e.preventDefault()
        $('.check_sms_btn').click()

    @pc.on 'click', '.send_sms_code', ->
      that.sendCode()

    @pc.on 'click', '.resend_sms_code:not(.done)', ->
      that.sendCode()

    @check_sms_btn.on 'click', ->
      smsCode = that.smsCode()
      that.checkCode(smsCode)

  showConfirmationInput: ->
    @pc.addClass('check_mode')

  closeConfirmationInput: ->
    @pc.removeClass('check_mode')

  showErrors: (data) ->
    @errors.addClass('open').text(data.responseJSON.message.alert)

  hideErrors: ->
    @errors.removeClass('open')

  smsCode: ->
    @pc.find('.check_sms_input').val()

  sendCode: ->
    that = this
    $.ajax
      url: '/phones/send_sms_code'
      type: 'GET'
      beforeSend: ->
        that.pc.addClass('before_send')
        that.hideErrors()
      success: (data) ->
        that.pc.removeClass('before_send')
        that.showConfirmationInput()
        that.smsTimer()
      error: (data) ->
        that.pc.removeClass('before_send')
        that.showErrors(data)

  checkCode: ->
    that = this
    $.ajax
      url: '/phones/' + @phone_id
      type: "PUT"
      data: { sms_code: that.smsCode() }
      beforeSend: ->
        that.check_sms_btn.addClass('loading')
        that.hideErrors()
      success: (data) ->
        that.check_sms_btn.removeClass('loading')
        that.pc.toggleClass('inactivated activated')
        that.closeConfirmationInput()
      error: (data) ->
        that.check_sms_btn.removeClass('loading')
        that.showErrors(data)

  smsTimer: ->
    i = 30
    link = $('.resend_sms_code')
    timer = link.find('.timer')
    timer.text(i)
    link.addClass('done')
    timerId = setInterval(->
      if i == 1
        clearInterval(timerId)
        link.removeClass('done')
        timer.text('')
      else
        i--
        timer.text(i)
    , 1000)

$ ->
  new App.PhoneConfirmation()
