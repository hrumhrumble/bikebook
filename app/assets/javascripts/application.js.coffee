#= require jquery
#= require jquery_ujs
#= require jquery-fileupload/basic
#= require init
#= require_tree ./vendor
#= require_tree ./app
#= require_tree ./shared
#= require_tree .
