window.App ||= {}

App.init = ->
  setTimeout(->
    $('.flash').slideUp(100)
  , 5000)

  $('.price_mask').mask('000 000 000 000 000', { reverse: true })

  $('#user_phone_attributes_number').mask '8 (000) 000-00-00',
    placeholder: "8 (000) 000-00-00"

  $('select').select2
    minimumResultsForSearch: -1
    width: '100%'
    
  $('.filter .city select').select2
    allowClear: true
    width: '100%'
    placeholder: "По всей России"
    language: {
      inputTooShort: -> "Введите название города"
      searching: -> 'Поиск...'
      noResults: -> 'Ничего не найдено'
    }
    ajax:
      url: '/cities/'
      dataType: 'json'
      delay: 250
      data: (params) ->
        {
          city: params.term
          page: params.page
        }
      processResults: (data) ->
        {results: data.results}
    minimumInputLength: 1

  $('.bike_new .city select').select2
    allowClear: true
    width: '100%'
    placeholder: "По всей России"
    language: {
      inputTooShort: -> "Введите название города"
      searching: -> 'Поиск...'
      noResults: -> 'Ничего не найдено'
    }
    ajax:
      url: '/cities/new'
      dataType: 'json'
      delay: 250
      data: (params) ->
        {
          city: params.term
          page: params.page
        }
      processResults: (data) ->
        {results: data.results}
    minimumInputLength: 1

$ ->
  App.init()
