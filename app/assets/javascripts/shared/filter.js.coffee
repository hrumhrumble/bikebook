sendForm = (that) ->
  form = $('.filter form')
  btn = form.find('.search')

  # city_select
  that.form.action = '/' + that.value + '/bikes' if $(that).attr('id') == 'city_id'
  that.form.action = '/' if $(that).attr('id') == 'city_id' && that.value == ''

  url = window.location.origin + form.attr('action') + '/?' + form.serialize()
  $.ajax
    url: url
    type: "GET"
    dataType: 'json'
    beforeSend: ->
      btn.attr('data-loading', 'true')
    success: (data)->
      btn.attr('data-loading', 'false')
      btn.attr('disabled', false).html(data.result)
      if data.size == 0
        btn.attr('disabled', true).text(data.result)
      else
        btn.find('span').text(data.result)
    error: ->
      btn.attr('data-loading', 'false')

init = ->
  $('.filter input[type=text]').on 'change', ->
    sendForm(@)

  $('.filter select, .filter input[type=checkbox]').on 'change', ->
    sendForm(@)

$ ->
  init()
