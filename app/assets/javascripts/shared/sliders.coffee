App.Sliders =->    
  fotorama = $('.fotorama')
  .on('fotorama:fullscreenenter', ->
    fotorama.setOptions({
      click: true
      arrows: 'always'
      fit: 'contain'
    })
  )
  .on('fotorama:fullscreenexit', ->
    fotorama.setOptions({
      click: false
      arrows: true
      fit: 'cover'
    })
  )
  .on('fotorama:ready', ->
    $('.fotorama__stage__shaft').on 'click', ->
      fotorama.requestFullScreen()
  )
  .fotorama(
    allowfullscreen: true
    loop: true
    fit: 'cover'
    nav: 'thumbs'
    width: '100%'
    click: false
    maxheight: '500px'
    ratio: "6/4"
    thumbwidth: '100px'
    thumbheight: '80px'
    thumbmargin: 10
  ).data('fotorama')


  $('.fotorama.without_thumbs').fotorama
    allowfullscreen: false
    loop: true
    nav: 'dots'
    width: '100%'
    ratio: "6/4"
    minheight: '190px'
    fit: 'cover'

  $('.fotorama.moderation').fotorama
    allowfullscreen: false
    loop: true
    nav: 'dots'
    width: '100%'
    ratio: "6/4"
    minheight: '190px'
    maxheight: '300px'

$ ->
  App.Sliders()
