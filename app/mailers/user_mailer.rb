class UserMailer < ApplicationMailer
  def approved(bike)
    @bike = bike
    @user = @bike.user
    mail(to: @user.email, subject: 'Ваше объявление одобрено')
  end

  def rejected(bike)
    @bike = bike
    @user = @bike.user
    mail(to: @user.email, subject: 'Ваше объявление отклонено')
  end
end
