class AdminMailer < ApplicationMailer
  def new_user
    mail(to: ENV['admin_email'], subject: 'Зарегестрировался новый пользователь!')
  end

  def on_moderation
    mail(to: ENV['admin_email'], subject: 'Добавлено новое объявление на модерацию')
  end

  def feedback(message)
    @message = message
    mail(to: ENV['admin_email'], subject: 'Предложение/жалоба')
  end
end
