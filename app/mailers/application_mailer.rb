class ApplicationMailer < ActionMailer::Base
  default from: "BikeBook.ru <info@bikebook.ru>"
end
