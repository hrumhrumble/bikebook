class Profiles::BikePolicy < ApplicationPolicy
  def initialize(user, bike)
    raise Pundit::NotAuthorizedError unless user
    @user = user
    @bike = bike.is_a?(Array) ? bike.last : bike
  end

  def index?
    @user.role.user? || @user.role.admin?
  end

  def create?
    @user.role.user? || @user.role.admin?
  end

  def new?
    @user.role.user? || @user.role.admin?
  end

  def show?
    @bike.user == @user || @user.role.admin?
  end

  def close?
    @bike.user == @user || @user.role.admin?
  end

  def restore?
    @bike.user == @user
  end

  def edit?
    @bike.user == @user || @user.role.admin?
  end

  def update?
    @bike.user == @user || @user.role.admin?
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope.is_a?(Array) ? scope.last : scope
    end

    def resolve
      if @user.present?
        @scope.where(user_id: @user.id)
      else
        raise Pundit::NotAuthorizedError unless @user
      end
    end
  end
end
