class Admin::ModerationPolicy < Struct.new(:user, :moderation)
  attr_reader :user, :bike

  def initialize(user, bike)
    raise Pundit::NotAuthorizedError unless user
    @user = user
    @bike = bike
  end

  def index?
    @user.role.admin?
  end

  def status_update?
    @user.role.admin?
  end
end
