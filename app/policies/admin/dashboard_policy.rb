class Admin::DashboardPolicy < Struct.new(:user, :dashboard)
  attr_reader :user, :bike

  def initialize(user, bike)
    raise Pundit::NotAuthorizedError unless user
    @user = user
    @bike = bike
  end

  def index?
    @user.role.admin?
  end
end
