class FeedbackPolicy < ApplicationPolicy
  def initialize(user, bike)
    raise Pundit::NotAuthorizedError unless user
    @user = user
    @bike = bike.is_a?(Array) ? bike.last : bike
  end

  def new?
    @user.role.user? || @user.role.admin?
  end

  def create?
    @user.role.user? || @user.role.admin?
  end
end
