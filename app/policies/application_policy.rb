class ApplicationPolicy
  attr_reader :user, :record

  def initialize(user, bike)
    @user = user
    @bike = bike.is_a?(Array) ? bike.last : bike
  end

  def admin?
    @user.present? && @user.role.admin?
  end
end
