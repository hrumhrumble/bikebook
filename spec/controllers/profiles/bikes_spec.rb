require 'rails_helper'

include Auth

describe Profiles::BikesController do
  statuses = Bike.status.values

  context 'User' do
    let(:user) { create(:user, :confirmed) }
    let(:foreign_user) { create(:user, :confirmed) }
    let(:bike) { create(:bike, user_id: user.id) }
    before(:each) { sign_in(user) }

    describe 'GET #index' do
      it 'render list of current_user bikes in any status' do
        owner_bikes   = []

        statuses.each do |status|
          owner_bikes << create(:bike, status.to_sym, user_id: user.id)
          create(:bike, status.to_sym, user_id: foreign_user.id)
        end

        get :index

        expect(assigns(:bikes)).to match_array(owner_bikes)
      end
    end

    describe 'GET #new' do
      it 'renders the :new template' do
        get :new
        expect(response).to render_template(:new)
      end
    end

    describe 'GET #show' do
      statuses.each do |status|
        it "renders the :show template of own bike in status: :#{status}" do
          bike = create(:bike, status.to_sym, user_id: user.id)

          get :show, id: bike
          expect(response).to render_template(:show)
        end
      end

      (statuses - ['approved']).each do |status|
        it "denied access to renders the :show template of foreign bike in status: :#{status}" do
          foreign_user = create(:user, :confirmed)
          foreign_bike = create(:bike, status.to_sym, user_id: foreign_user.id)

          get :show, id: foreign_bike
          expect(response).to_not render_template(:show)
        end
      end
    end

    describe 'GET #edit' do
      statuses.each do |status|
        it "renders the :edit template in status: :#{status}" do
          bike = create(:bike, status.to_sym, user_id: user.id)

          get :edit, id: bike
          expect(response).to render_template(:edit)
        end
      end

      statuses.each do |status|
        it "denied access to renders the :edit template of foreign bike in status: :#{status}" do
          foreign_user = create(:user, :confirmed)
          foreign_bike = create(:bike, status.to_sym, user_id: foreign_user.id)

          get :edit, id: foreign_bike
          expect(response).to_not render_template(:edit)
        end
      end
    end

    describe 'POST #create' do
      it 'can create bike'
    end

    describe 'POST #update' do
      it 'can update bike in any status'
    end

    describe 'POST #restore' do
      statuses.each do |status|
        it "from status: :#{status}" do
          bike = create(:bike, status.to_sym, user_id: user.id)

          post :restore, id: bike
          bike.reload

          expect(bike.status).to eq(:on_moderation)
        end
      end

      statuses.each do |status|
        it "denied access to foreign bike with status: :#{status}" do
          foreign_user = create(:user, :confirmed)
          foreign_bike = create(:bike, status.to_sym, user_id: foreign_user.id)

          post :restore, id: foreign_bike
          foreign_bike.reload

          expect(foreign_bike.status).to eq(status)
        end
      end
    end

    describe 'POST #close' do
      statuses.each do |status|
        it "from status: :#{status}" do
          bike = create(:bike, status.to_sym, user_id: user.id)
          post :close, id: bike.id
          bike.reload
          expect(bike.status).to eq(:closed)
        end
      end

      statuses.each do |status|
        it "denied access to foreign bike with status: :#{status}" do
          foreign_user = create(:user, :confirmed)
          foreign_bike = create(:bike, status.to_sym, user_id: foreign_user.id)

          post :close, id: foreign_bike
          foreign_bike.reload

          expect(foreign_bike.status).to eq(status)
        end
      end
    end
  end

  context 'Guest' do
    describe 'GET #index' do
      it 'denied access to renders the :index template' do
        get :new
        expect(response).to_not render_template(:index)
      end
    end

    describe 'GET #new' do
      it 'denied access to renders the :new template' do
        get :new
        expect(response).to_not render_template(:new)
      end
    end

    describe 'GET #edit' do
      statuses.each do |status|
        it "denied access to renders the :edit template in status: :#{status}" do
          bike = create(:bike, status.to_sym)

          get :edit, id: bike
          expect(response).to_not render_template(:edit)
        end
      end
    end

    describe 'POST #restore' do
      statuses.each do |status|
        it "denied access with status: :#{status}" do
          bike = create(:bike, status.to_sym)

          post :restore, id: bike
          bike.reload

          expect(bike.status).to eq(status)
        end
      end
    end

    describe 'POST #close' do
      statuses.each do |status|
        it "denied access with status: :#{status}" do
          bike = create(:bike, status.to_sym)

          post :close, id: bike
          bike.reload

          expect(bike.status).to eq(status)
        end
      end
    end
  end
end
