require 'rails_helper'

include Auth

describe Admin::ModerationController do
  statuses = Bike.status.values

  context 'Admin' do
    let(:user) { create(:user, :admin, :confirmed) }
    before(:each) { sign_in(user) }

    it 'renders the :index template' do
      get :index
      expect(response).to render_template(:index)
    end
  end

  context 'User' do
    let(:user) { create(:user, :confirmed) }
    let(:foreign_user) { create(:user, :confirmed) }
    let(:bike) { create(:bike, user_id: user.id) }
    before(:each) { sign_in(user) }

    it 'denied access to renders the :index template' do
      get :index
      expect(response).to_not render_template(:index)
    end

    describe 'POST #approve' do
      statuses.each do |status|
        it "denied access with bike status: :#{status}" do
          bike = create(:bike, status.to_sym, user_id: user.id)
          post :status_update, id: bike
          bike.reload
          expect(bike.status).to eq(status)
        end
      end

      statuses.each do |status|
        it "denied access to foreign bike with status: :#{status}" do
          foreign_user = create(:user, :confirmed)
          foreign_bike = create(:bike, status.to_sym, user_id: foreign_user.id)

          post :status_update, id: foreign_bike
          foreign_bike.reload

          expect(foreign_bike.status).to eq(status)
        end
      end
    end

    describe 'POST #reject' do
      statuses.each do |status|
        it "denied access with bike status: :#{status}" do
          bike = create(:bike, status.to_sym, user_id: user.id)
          post :status_update, id: bike
          bike.reload
          expect(bike.status).to eq(status)
        end
      end

      statuses.each do |status|
        it "denied access to foreign bike with status: :#{status}" do
          foreign_user = create(:user, :confirmed)
          foreign_bike = create(:bike, status.to_sym, user_id: foreign_user.id)

          post :status_update, id: foreign_bike
          foreign_bike.reload

          expect(foreign_bike.status).to eq(status)
        end
      end
    end

    describe 'POST #company' do
      statuses.each do |status|
        it "denied access with bike status: :#{status}" do
          bike = create(:bike, status.to_sym, user_id: user.id)
          post :status_update, id: bike
          bike.reload
          expect(bike.user.is_company).to eq(false)
        end
      end

      statuses.each do |status|
        it "denied access to foreign bike with status: :#{status}" do
          foreign_user = create(:user, :confirmed)
          foreign_bike = create(:bike, status.to_sym, user_id: foreign_user.id)

          post :status_update, id: foreign_bike
          foreign_bike.reload

          expect(foreign_bike.user.is_company).to eq(false)
        end
      end
    end
  end

  context 'Guest' do
    it 'denied access to renders the :index template' do
      get :index
      expect(response).to_not render_template(:index)
    end

    describe 'POST #approve' do
      statuses.each do |status|
        it "denied access with bike status: :#{status}" do
          bike = create(:bike, status.to_sym)

          post :status_update, id: bike
          bike.reload

          expect(bike.status).to eq(status)
        end
      end
    end

    describe 'POST #reject' do
      statuses.each do |status|
        it "denied access with bike status: :#{status}" do
          bike = create(:bike, status.to_sym)

          post :status_update, id: bike
          bike.reload

          expect(bike.status).to eq(status)
        end
      end
    end

    describe 'POST #company' do
      statuses.each do |status|
        it "denied access with bike status: :#{status}" do
          bike = create(:bike, status.to_sym)

          post :status_update, id: bike
          bike.reload

          expect(bike.user.is_company).to eq(false)
        end
      end
    end
  end
end
