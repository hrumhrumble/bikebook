require 'rails_helper'

include Auth

describe Admin::DashboardController do
  context 'Admin' do
    let(:user) { create(:user, :admin, :confirmed) }
    before(:each) { sign_in(user) }

    it 'renders the :index template' do
      get :index
      expect(response).to render_template(:index)
    end
  end

  context 'User' do
    let(:user) { create(:user, :confirmed) }
    before(:each) { sign_in(user) }

    it 'denied access to renders the :index template' do
      get :index
      expect(response).to_not render_template(:index)
    end
  end

  context 'Guest' do
    it 'denied access to renders the :index template' do
      get :index
      expect(response).to_not render_template(:index)
    end
  end
end
