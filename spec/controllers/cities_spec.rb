require 'rails_helper'

include Auth

describe CitiesController do
  describe 'GET #index' do
    it 'renders the :index template' do
      get :show
      expect(response).to render_template(:show)
    end

    it 'renders only approved without companies bikes' do
      user = create(:user, :is_company)
      approved = create(:bike, :approved)
      create(:bike, :on_moderation)
      create(:bike, :rejected)
      create(:bike, :closed)
      create(:bike, :approved, user_id: user.id)

      get :show

      expect(assigns(:bikes)).to match_array(approved)
    end

    it 'filtered where city_id' do
      city_first = create(:city)
      city_second = create(:city)

      create(:bike, :approved, city: city_second)
      bike = create(:bike, :approved, city: city_first)

      get :show, city_id: city_first.slug

      expect(assigns(:bikes)).to match_array(bike)
    end

    it 'filtered where function' do
      road = create(:bike, :approved, function: :road)
      create(:bike, :approved, function: :mtb)

      get :show, function: 'road'

      expect(assigns(:bikes)).to match_array(road)
    end

    it 'filtered where folding' do
      folding = create(:bike, :approved, folding: true)
      create(:bike, :approved, folding: false)

      get :show, folding: true

      expect(assigns(:bikes)).to match_array(folding)
    end

    it 'filtered where electric' do
      electric = create(:bike, :approved, electric: true)
      create(:bike, :approved, electric: false)

      get :show, electric: true

      expect(assigns(:bikes)).to match_array(electric)
    end

    it 'filtered where suspension' do
      hard_tail = create(:bike, :approved, suspension: :hard_tail)
      create(:bike, :approved, suspension: :no_suspension)

      get :show, suspension: 'hard_tail'

      expect(assigns(:bikes)).to match_array(hard_tail)
    end

    it 'filtered where price_from' do
      price_1000 = create(:bike, :approved, price: 1000)
      price_500 = create(:bike, :approved, price: 500)

      get :show, price_from: 0

      expect(assigns(:bikes)).to match_array([price_1000, price_500])
    end

    it 'filtered where price_to' do
      price_1000 = create(:bike, :approved, price: 1000)
      price_500 = create(:bike, :approved, price: 500)
      price_250 = create(:bike, :approved, price: 250)

      get :show, price_to: 500

      expect(assigns(:bikes)).to match_array([price_250, price_500])
    end

    it 'sorted by price DESC' do
      price_1000 = create(:bike, :approved, price: 1000)
      price_500 = create(:bike, :approved, price: 500)
      price_250 = create(:bike, :approved, price: 250)

      get :show, sort: :expensive

      expect(assigns(:bikes)).to eq([price_1000, price_500, price_250])
    end

    it 'sorted by price ASC' do
      price_1000 = create(:bike, :approved, price: 1000)
      price_500 = create(:bike, :approved, price: 500)
      price_250 = create(:bike, :approved, price: 250)

      get :show, sort: :cheap

      expect(assigns(:bikes)).to eq([price_250, price_500, price_1000])
    end

    it 'sorted by created_at ASC' do
      first = create(:bike, :approved, created_at: Time.now)
      second = create(:bike, :approved, created_at: Time.now - 1.day)
      last = create(:bike, :approved, created_at: Time.now - 2.day)

      get :show, sort: nil

      expect(assigns(:bikes)).to eq([first, second, last])
    end
  end
end
