require 'rails_helper'

include Auth

describe BikesController do
  describe 'GET #phone' do
    it 'render json phone' do
      bike = create(:bike, :approved)
      phone = { number: bike.phone }.to_json

      get :phone, id: bike.id

      expect(response.body).to eq(phone)
    end
  end

  describe 'GET #email' do
    it 'render json email' do
      bike = create(:bike, :approved)
      email = { email: bike.email }.to_json

      get :email, id: bike.id

      expect(response.body).to eq(email)
    end
  end

  describe 'GET #show' do
    it 'render the :show template if bike status approved and bike user not company' do
      bike = create(:bike, :approved)

      get :show, city_id: bike.city.id, id: bike
      expect(response).to render_template(:show)
    end

    it 'not render the :show template if bike status approved and bike user is company' do
      user = create(:user, :is_company)
      bike = create(:bike, :approved, user_id: user.id)

      get :show, city_id: bike.city.id, id: bike
      expect(response).to_not render_template(:show)
    end

    (Bike.status.values - ['approved']).each do |status|
      it "not render the :show template in status: :#{status}" do
        bike = create(:bike, status.to_sym)

        get :show, city_id: bike.city.id, id: bike
        expect(response).to_not render_template(:show)
      end
    end
  end
end
