require 'rails_helper'

include Auth

describe FeedbackController do
  context 'User' do
    let(:user) { create(:user, :confirmed) }
    before(:each) { sign_in(user) }

    describe 'GET #new' do
      it 'renders the :index template' do
        get :new
        expect(response).to render_template(:new)
      end
    end

    describe 'POST #create' do
      it 'create feedback' do
        post :create, feedback: { title: 'Title', description: 'Description', user_id: user.id }
        expect(Feedback.count).to eq(1)
      end
    end
  end

  context 'Guest' do
    describe 'GET #new' do
      it 'cannot access to new template' do
        get :new
        expect(response).to_not render_template(:new)
      end
    end

    describe 'POST #create' do
      it 'cannot create feedback' do
        post :create, feedback: { title: 'Title', description: 'Description', user_id: nil }
        expect(Feedback.count).to eq(0)
      end
    end
  end
end
