require 'rails_helper'

describe User do
  it { should enumerize(:role) }
  it { should have_many(:bikes).dependent(:destroy) }
  it { should have_many(:feedbacks).dependent(:destroy) }
  it { should have_one(:phone).dependent(:destroy) }
  it { should accept_nested_attributes_for(:phone).allow_destroy(true) }
  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:name) }
end

