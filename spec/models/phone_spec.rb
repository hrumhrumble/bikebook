require 'rails_helper'

describe Phone do
  context 'validations' do
    it { should belong_to(:user) }
    it { should validate_presence_of(:number) }

    it 'validate what :number not uniq if activated: true, sms_key: nil (mean parsed)' do
      create(:phone, :activated, number: '8 (915) 000-00-00')
      expect(build(:phone, number: '8 (915) 000-00-00')).to be_valid
    end

    it 'validate what :number not uniq if activated: false' do
      create(:phone, number: '8 (915) 000-00-00')
      expect(build(:phone, number: '8 (915) 000-00-00')).to be_valid
    end

    it 'validate what :number not uniq if activated: true, sms_key: key (mean user)' do
      create(:phone, :activated, number: '8 (915) 000-00-00', sms_key: 'key')
      expect(build(:phone, number: '8 (915) 000-00-00')).to_not be_valid
    end
  end

  it '#phone_format' do
    phone = create(:phone, number: '8 (915) 000-00-00')
    expect(phone.number).to eq('89150000000')
  end

  it '#drop_activation' do
    phone = create(:phone, number: '8 (915) 000-00-00')
    phone.update(number: '8 (915) 000-00-01')
    expect(phone.activated).to eq(false)
  end
end

