require 'rails_helper'

describe Bike do
  it { should belong_to(:user) }
  it { should belong_to(:city) }

  it { should enumerize(:status) }
  it { should enumerize(:function) }
  it { should enumerize(:suspension) }
  it { should enumerize(:gears) }
  it { should enumerize(:wheels) }
  it { should enumerize(:age) }

  it { should have_many(:pictures).dependent(:destroy) }
  it { should accept_nested_attributes_for(:pictures).allow_destroy(true) }

  it { should validate_presence_of(:title) }
  it { should validate_presence_of(:city_id) }
  it { should validate_presence_of(:function) }
  it { should validate_presence_of(:price) }
  it { should validate_presence_of(:pictures) }

  it 'default status' do
    bike = build(:bike)
    expect(bike.status).to eq(:on_moderation)
  end

  context 'Contacts' do
    let(:bike) { create(:bike) }

    it '#phone' do
      expect(bike.phone).to eq(bike.user.phone.number)
    end

    it '#email' do
      expect(bike.email).to eq(bike.user.email)
    end
  end

  describe '#drop_moderation' do
    shared_examples 'drop to :on_moderation' do
      it 'when title was changed' do
        bike.update(title: 'New Title')
        expect(bike.status).to eq(:on_moderation)
      end

      it 'when description was changed' do
        bike.update(description: 'New Description')
        expect(bike.status).to eq(:on_moderation)
      end

      # it 'when new pictures added' do
      #   bike.pictures << create(:picture)
      #   bike.save
      #   expect(bike.status).to eq(:on_moderation)
      # end
      #
      # it 'when exist pictures updated' do
      #   bike.pictures.destroy_all
      #   bike.pictures << create_list(:picture, 3)
      #   bike.save
      #   expect(bike.status).to eq(:on_moderation)
      # end
    end

    shared_examples 'not drop to :on_moderation' do
      it 'when pictures removed' do
        bike.pictures.destroy_all
        bike.save
        expect(bike.status).to_not eq(:on_moderation)
      end
    end

    context 'from :closed' do
      it_behaves_like 'drop to :on_moderation' do
        let(:bike) { create(:bike, :closed) }
      end

      it_behaves_like 'not drop to :on_moderation' do
        let(:bike) { create(:bike, :closed) }
      end
    end

    context 'from :on_moderation' do
      it_behaves_like 'drop to :on_moderation' do
        let(:bike) { create(:bike, :on_moderation) }
      end
    end

    context 'from :approved' do
      it_behaves_like 'drop to :on_moderation' do
        let(:bike) { create(:bike, :approved) }
      end

      it_behaves_like 'not drop to :on_moderation' do
        let(:bike) { create(:bike, :approved) }
      end
    end

    context 'from :rejected' do
      it_behaves_like 'drop to :on_moderation' do
        let(:bike) { create(:bike, :rejected) }
      end

      it_behaves_like 'not drop to :on_moderation' do
        let(:bike) { create(:bike, :rejected) }
      end
    end
  end
end
