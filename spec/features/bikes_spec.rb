require 'rails_helper'

include Auth

feature 'Bikes' do
  let(:user) { create(:user, :confirmed, phone: create(:phone, :activated, number: '80000000000')) }
  let(:bike) { create(:bike, :approved, user_id: user.id) }

  scenario 'show phone', js: true do
    visit bike_path(bike.city, bike)
    page.execute_script("$('[data-behavior=show_contact_phone]').click()")

    expect(page).to have_content('8 (000) 000-00-00')
  end

  scenario 'show email', js: true do
    visit bike_path(bike.city, bike)
    page.execute_script("$('[data-behavior=show_contact_email]').click()")

    expect(page).to have_content(bike.email)
  end
end
