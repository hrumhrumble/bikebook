require 'rails_helper'

include Auth

feature 'Authentication' do
  context 'User' do
    scenario 'confirmed can sign in' do
      login(create(:user, :confirmed))

      visit edit_user_registration_path
      expect(page).to have_content('Мой профиль')
    end

    scenario "no confirmed can't sign in" do
      login(create(:user))

      visit edit_user_registration_path
      expect(page.current_path).to eq(new_user_session_path)
    end
  end
end
