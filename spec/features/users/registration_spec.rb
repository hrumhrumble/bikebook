require 'rails_helper'

feature 'Registration' do
  context 'Guest' do
    include EmailSpec::Helpers
    include EmailSpec::Matchers

    context 'with valid details' do
      scenario 'all fields valid', js: true do
        user = build(:user)

        visit new_user_registration_path
        fill_in 'user[name]', with: user.name
        fill_in 'user[email]', with: user.email
        fill_in 'user[password]', with: user.password
        fill_in 'user[password_confirmation]', with: user.password_confirmation
        fill_in 'user[phone_attributes][number]', with: user.phone.number
        find('input[type=submit]').click
        open_email(user.email)
        visit_in_email('Подтвердить почту')
        expect(current_path).to eq new_user_session_path
        expect(page).to have_content("Ваш адрес электронной почты успешно подтвержден.")

        fill_in 'user[email]', with: user.email
        fill_in 'user[password]', with: user.password
        find('input[type=submit]').click

        expect(page).to have_content("Мои объявления")
      end

      scenario 'when phone is already exist, activated: false' do
        exist_phone = create(:phone)
        user = build(:user)

        visit new_user_registration_path

        fill_in 'user[name]', with: user.name
        fill_in 'user[email]', with: user.email
        fill_in 'user[password]', with: user.password
        fill_in 'user[password_confirmation]', with: user.password
        fill_in 'user[phone_attributes][number]', with: exist_phone.number
        find('input[type=submit]').click
        expect(page).to have_content('с инструкциями по подтверждению Вашей учётной записи.')
      end
    end

    context 'with invalid details' do
      scenario 'blank fields' do
        visit new_user_registration_path
        find('input[type=submit]').click
        expect(page).to have_content('не может быть пустым')
      end

      scenario 'wrong password confirmation' do
        user = build(:user)
        visit new_user_registration_path

        fill_in 'user[name]', with: user.name
        fill_in 'user[email]', with: user.email
        fill_in 'user[password]', with: user.password
        fill_in 'user[password_confirmation]', with: 'wrong_password_confirmation'
        fill_in 'user[phone_attributes][number]', with: user.phone.number
        find('input[type=submit]').click
        within '.user_password_confirmation' do
          expect(page).to have_content('не совпадает со значением поля Пароль')
        end
      end

      scenario 'less when 8 symbols' do
        user = build(:user, password: 'less_8')
        visit new_user_registration_path

        fill_in 'user[name]', with: user.name
        fill_in 'user[email]', with: user.email
        fill_in 'user[password]', with: user.password
        fill_in 'user[password_confirmation]', with: user.password
        fill_in 'user[phone_attributes][number]', with: user.phone.number
        find('input[type=submit]').click
        within '.user_password' do
          expect(page).to have_content('недостаточной длины')
        end
      end

      scenario 'without phone' do
        user = build(:user)
        visit new_user_registration_path

        fill_in 'user[name]', with: user.name
        fill_in 'user[email]', with: user.email
        fill_in 'user[password]', with: user.password
        fill_in 'user[password_confirmation]', with: user.password
        fill_in 'user[phone_attributes][number]', with: ''
        find('input[type=submit]').click
        within '.user_phone_number' do
          expect(page).to have_content('не может быть пустым')
        end
      end

      scenario 'without name' do
        user = build(:user)
        visit new_user_registration_path

        fill_in 'user[name]', with: ''
        fill_in 'user[email]', with: user.email
        fill_in 'user[password]', with: user.password
        fill_in 'user[password_confirmation]', with: user.password
        fill_in 'user[phone_attributes][number]', with: user.password
        find('input[type=submit]').click
        within '.user_name' do
          expect(page).to have_content('не может быть пустым')
        end
      end

      scenario 'when phone is already exist and activated: true and sms_key is not nil (mean user phone, not parsed)' do
        activated_phone = create(:phone, activated: true, sms_key: 'key')
        user = build(:user)

        visit new_user_registration_path

        fill_in 'user[name]', with: user.name
        fill_in 'user[email]', with: user.email
        fill_in 'user[password]', with: user.password
        fill_in 'user[password_confirmation]', with: user.password
        fill_in 'user[phone_attributes][number]', with: activated_phone.number
        find('input[type=submit]').click
        within '.user_phone_number' do
          expect(page).to have_content('уже существует')
        end
      end

      scenario 'when email is already exist' do
        exist_user = create(:user)
        user = build(:user)

        visit new_user_registration_path

        fill_in 'user[name]', with: user.name
        fill_in 'user[email]', with: exist_user.email
        fill_in 'user[password]', with: user.password
        fill_in 'user[password_confirmation]', with: user.password
        fill_in 'user[phone_attributes][number]', with: user.phone.number
        find('input[type=submit]').click
        within '.user_email' do
          expect(page).to have_content('уже существует')
        end
      end
    end
  end
end
