require 'rails_helper'

feature 'Favorites' do
  let!(:user) { create(:user, :confirmed, phone: create(:phone, :activated, number: '80000000000')) }
  let!(:bike) { create(:bike, :approved, user_id: user.id) }

  scenario 'add to favorites bike#show', js: true do
    visit bike_path(bike.city, bike)
    page.execute_script("$('.add_to_favorites').click()")
    visit favorites_path
    expect(page).to have_content(bike.city.name)
  end

  scenario 'add to favorites bike#index', js: true do
    visit root_path
    page.execute_script("$('[data-behavior=add_to_favorites]').click()")
    visit favorites_path
    expect(page).to have_content(bike.city.name)
  end

  scenario 'remove from favorites', js: true do
    visit bike_path(bike.city, bike)
    page.execute_script("$('[data-behavior=add_to_favorites]').click()")
    visit favorites_path
    page.execute_script("$('[data-behavior=add_to_favorites]').click()")
    visit favorites_path
    expect(page).to_not have_content(bike.city.name)
  end
end
