require 'rails_helper'

include Auth

feature 'Bikes' do
  let(:user) { create(:user, :confirmed) }
  let(:city) { create(:city) }
  before(:each) { login(user) }

  scenario 'create bike with valid data', js: true do
    bike = build(:bike)

    visit new_profiles_bike_path
    fill_in 'bike[title]', with: bike.title
    fill_in 'bike[description]', with: bike.title
    select bike.gears, from: 'bike[gears]'
    select bike.wheels, from: 'bike[wheels]'
    select I18n.t("enumerize.defaults.suspension.#{bike.suspension}"), from: 'bike[suspension]'
    page.execute_script("$('.city select').select2('open')")
    find('.select2-search__field').set(city.name)
    sleep 2 # для подгрузки города в инпут необходимо время
    find('.select2-search__field').set("\n")
    select I18n.t("enumerize.defaults.function.#{bike.function}"), from: 'bike[function]'
    select I18n.t("enumerize.defaults.age.#{bike.age}"), from: 'bike[age]'
    select I18n.t("enumerize.defaults.sex.#{bike.sex}"), from: 'bike[sex]'
    find(:css, '#bike_folding').set(bike.folding)
    find(:css, '#bike_electric').set(bike.electric)
    fill_in 'bike[price]', with: bike.price
    page.execute_script("$('#bike_pictures').show()")
    page.attach_file('bike_pictures', Rails.root + 'public/bike.png')
    click_button 'Сохранить'
    expect(page.current_path).to eq(profiles_bikes_path)
    expect(page).to have_content(bike.title)
  end

  scenario 'create bike with empty fields' do
    visit new_profiles_bike_path
    click_button 'Сохранить'

    expect(page.current_path).to eq(profiles_bikes_path)
    expect(page).to have_content('не может быть пустым')
  end

  scenario 'edit bike' do
    bike = create(:bike, user_id: user.id)

    visit edit_profiles_bike_path(bike)
    fill_in 'bike[title]', with: 'Changed title'
    click_button 'Сохранить'

    expect(page.current_path).to eq(profiles_bikes_path)
    expect(page).to have_content('Changed title')
  end
end
