module Auth
  def login(user)
    visit new_user_session_path
    fill_in 'user[email]', with: user.email
    fill_in 'user[password]', with: user.password
    find('input[type=submit]').click
  end

  def logout
    visit profiles_bikes_path
    find('#destroy_user_session').click
  end
end
