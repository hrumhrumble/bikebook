FactoryGirl.define do

  factory :department do
    name { FFaker::Name.name }
  end
end
