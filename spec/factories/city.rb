FactoryGirl.define do

  factory :city do
    name { FFaker::Address.city.to_param }
    slug { FFaker::Address.city.to_param }

    association :department
  end
end
