FactoryGirl.define do
  factory :bike do
    title { FFaker::Movie.title }
    description { FFaker::Lorem.paragraph }
    function { Bike.function.values.sample }
    wheels { Bike.wheels.values.sample }
    gears { Bike.gears.values.sample }
    suspension { Bike.suspension.values.sample }
    age { Bike.age.values.sample }
    sex { Bike.sex.values.sample }
    folding { [true, false].sample }
    electric { [true, false].sample }
    price { rand(100..99999) }
    created_at { Time.now - rand(1..90).days - rand(1..23).hours - rand(1..59).minutes }

    after(:build) do |bike, evaluator|
      bike.pictures << ( build_list(:picture, 3, bike: bike) )
    end

    trait :on_moderation do
      status { :on_moderation }
    end

    trait :approved do
      status { :approved }
    end

    trait :rejected do
      status { :rejected }
    end

    trait :closed do
      status { :closed }
    end

    trait :with_original_url do
      original_url { 'http://original.ru' }
    end

    association :user
    association :city
  end
end
