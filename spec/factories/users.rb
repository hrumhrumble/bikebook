FactoryGirl.define do
  factory :user do
    name { FFaker::Name.name }
    email { FFaker::Internet.email }
    password '12345678'
    password_confirmation '12345678'

    trait :confirmed do
      confirmed_at { Time.now }
    end

    trait :admin do
      role 'admin'
    end

    trait :is_company do
      is_company true
    end

    association :phone
  end
end
