FactoryGirl.define do

  factory :phone do
    number { "8 (#{rand(000..999)}) #{rand(000..999)}-02-00" }

    trait :activated do
      activated { true }
    end
  end
end
